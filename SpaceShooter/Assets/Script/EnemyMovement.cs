﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.AI;
using UnityEngine;

public class EnemyMovement : MonoBehaviour {

    public GameObject Core;
    NavMeshAgent agent;
	// Use this for initialization
	void Start () {
        agent = GetComponent<NavMeshAgent>();
	}
	
	// Update is called once per frame
	void Update () {
        agent.SetDestination(Core.transform.position);
        //float dist = Vector3.Distance(agent.transform.position,Core.transform.position);
        //if(dist < 1)
        //{
        //    Debug.Log("Core");
        //}
    }
}
