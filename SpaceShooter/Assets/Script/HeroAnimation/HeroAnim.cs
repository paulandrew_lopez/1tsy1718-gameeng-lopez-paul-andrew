﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum AnimationType
{
    Idle = 0,
    Run = 1,
    Attack = 2,
    Dead = 3
}

public class HeroAnim : MonoBehaviour
{
    Animator anim;
    [SerializeField]AnimationType animType = AnimationType.Idle;

    // Use this for initialization
    void Start()
    {
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        //if (Input.GetKeyDown(KeyCode.UpArrow))
        //{
        //    OnChangeAnim(AnimationType.Idle);
        //}
        //if (Input.GetKeyDown(KeyCode.DownArrow))
        //{
        //    OnChangeAnim(AnimationType.Run);
        //}
        //if (Input.GetKeyDown(KeyCode.RightArrow))
        //{
        //    OnChangeAnim(AnimationType.Attack);
        //}
        //if (Input.GetKeyDown(KeyCode.LeftArrow))
        //{
        //    OnChangeAnim(AnimationType.Dead);
        //}
    }
    void OnChangeAnim(AnimationType animType)
    {
        anim.SetInteger("AnimIndex", (int)animType);
    }
}
