﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class WaveData
{


}

public class Spawner : MonoBehaviour
{
    public enum MonsterType
    {
        Flying,
        Ground,
    }
    public enum State
    {
        Spawning,
        Waiting,
        Counting
    }
    [System.Serializable]
    public class MonsterData
    {
        public string name;
        public MonsterType type;
        public List<GameObject> monsterPrefab;
        public float monsterHP;
        public int gold;

    }

    //what wave u are at
    public int Wave;
    //what wave u want to spawn the boss in
    public int BossWave;
    public GameObject Spawn;
    public GameObject Monster;
    public GameObject Boss;
    public float TimeTillNextWave;
    public float DefaultTimeForWave;
    //public List<MonsterData> monsterList = new List<MonsterData>();
    //public List<GameObject> MonsterWave = new List<GameObject>();
    public MonsterData[] waves;

    private State state = State.Counting;
    private bool Spawned = false;
    // Use this for initialization
    void Start()
    {
        TimeTillNextWave = DefaultTimeForWave;
    }

    IEnumerator SpawnMonster(MonsterData mons)
    {
        state = State.Spawning;

        if (Wave % BossWave == 0 && Spawned == false)
        {
            Spawned = true;
            mons.monsterPrefab.Add(Boss);
        }
        for (int i = 0; i < mons.monsterPrefab.Count; i++)
        {
            Instantiate(mons.monsterPrefab[i], this.transform.position, Quaternion.identity);
            yield return new WaitForSeconds(0.5f);
        }

    }

    // Update is called once per frame
    void Update()
    {

        if (TimeTillNextWave <= 0)
        {

            if (state != State.Spawning)
            {
                Wave++;
                StartCoroutine(SpawnMonster(waves[Wave]));

                TimeTillNextWave = DefaultTimeForWave;
            }

        }
        else
        {

            TimeTillNextWave -= Time.deltaTime;
        }
    }
}