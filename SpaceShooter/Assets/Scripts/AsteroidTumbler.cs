﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidTumbler : MonoBehaviour {

    public float Tumble;

    private Rigidbody rb;

    // Use this for initialization
    void Start () {
        rb = GetComponent<Rigidbody>();
        rb.angularVelocity = Random.insideUnitSphere * Tumble;
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
