﻿using UnityEngine;
using System.Collections;

public class BGScroller : MonoBehaviour
{

    public float Speed;
    public float TileY;

    private Vector3 startPosition;

    void Start()
    {
        startPosition = transform.position;
    }

    void Update()
    {
        float newPosition = Mathf.Repeat(Time.time * Speed, TileY);
        transform.position = startPosition + Vector3.up * newPosition;
    }
}