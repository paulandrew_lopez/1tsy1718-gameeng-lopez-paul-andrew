﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletMover : MonoBehaviour {
    public float speed;
    Rigidbody rb;
    // Use this for initialization
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        rb.velocity = Vector3.up * speed;
        Destroy(this.gameObject, 4);
    }
    void OnTriggerEnter(Collider col)
    {
        if (!col.gameObject.name.Contains("Player"))
        {
            Destroy(this.gameObject);
        }
    }
}
