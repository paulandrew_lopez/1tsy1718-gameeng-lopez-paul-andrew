﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{

    // Use this for initialization
    public GameObject Asteroids;
    public GameObject PowerUpObj;
    public Vector3 SpawnValue;
    public float SpawnWaitPowerUp;
    public float SpawnWaitAsteroid;
    public float StartWait;
    public GameObject Ship;

    private GameObject SimpleShip;
    void Start()
    {
        //SimpleShip = (GameObject)Instantiate(Ship);
        StartCoroutine(SpawnAsteroids());
        StartCoroutine(PowerUp());
    }
    private void Update()
    {
        if (SimpleShip == null)
        {
            Debug.Log("NULL");
            SimpleShip = (GameObject)Instantiate(Ship);
        }
    }
    IEnumerator SpawnAsteroids()
    {
        for (;;)
        {
            Vector3 spawnPosition = new Vector3(Random.Range(-SpawnValue.x, SpawnValue.x), SpawnValue.y, SpawnValue.z);
            Quaternion spawnRotation = Quaternion.identity;
            Asteroids = (GameObject)Instantiate(Asteroids, spawnPosition, spawnRotation);
            yield return new WaitForSeconds(SpawnWaitAsteroid);
            Destroy(Asteroids.gameObject, 4.5f);
        }
    }
    IEnumerator PowerUp()
    {

        for (;;)
        {
            Vector3 spawnPosition = new Vector3(Random.Range(-SpawnValue.x, SpawnValue.x), SpawnValue.y, SpawnValue.z);
            Quaternion spawnRotation = Quaternion.identity;
            PowerUpObj = (GameObject)Instantiate(PowerUpObj, spawnPosition, spawnRotation);
            yield return new WaitForSeconds(SpawnWaitPowerUp);
            Destroy(PowerUpObj.gameObject, 4.5f); 
        }
    }
}
