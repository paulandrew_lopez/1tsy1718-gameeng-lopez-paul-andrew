﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class PlayerGameOver : MonoBehaviour {
    public PlayerMovement Ship;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(Ship.Lives <= 0)
        {
            SceneManager.LoadScene("GameOverScene");
        }
	}
}
