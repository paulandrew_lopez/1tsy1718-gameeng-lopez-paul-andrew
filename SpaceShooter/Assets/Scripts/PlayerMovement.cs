﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float Speed;
    public GameObject Projectile;
    public float xMin, xMax, yMin, yMax;
    public GameObject ShotSpawnLeft;
    public GameObject ShotSpawnRight;
    public float FireRate;
    public float Lives;

    private GameObject ProjectileObjRight;
    private GameObject ProjectileObjLeft;
    private float nextShot;
    private Vector3 movement = Vector3.zero;
    private Rigidbody rb;

    // Use this for initialization
    void Start()
    {

        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {     
        if (Input.GetButton("Fire1") && Time.time > nextShot)
        {
            nextShot = Time.time + FireRate;
            ProjectileObjLeft = (GameObject)Instantiate(Projectile, ShotSpawnLeft.transform.position, Quaternion.identity);

            ProjectileObjRight = (GameObject)Instantiate(Projectile, ShotSpawnRight.transform.position, Quaternion.identity);

            Destroy(ProjectileObjLeft.gameObject, 0.5f);
            Destroy(ProjectileObjRight.gameObject, 0.5f);
           
        }      
        float Horizontal = Input.GetAxis("Horizontal");
        float Vertical = Input.GetAxis("Vertical");

        movement = new Vector3(Horizontal, Vertical, 0);

        rb.velocity = movement * Speed * Time.deltaTime;  
        rb.position = new Vector3(
           Mathf.Clamp(this.transform.position.x, xMin, xMax),
           Mathf.Clamp(this.transform.position.y, yMin, yMax),
           this.transform.position.z);
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name.Contains("Asteroid"))
        {
            Debug.Log("Collided");
            Destroy(other.gameObject);
            Lives--;
        }
        if (other.gameObject.name.Contains("Power Up"))
        {
            Debug.Log("Collided Power UP");
            Destroy(other.gameObject);
            Lives++;
        }
    }
}
