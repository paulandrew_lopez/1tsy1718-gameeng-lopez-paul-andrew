﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class PlayerRespawn : MonoBehaviour
{
    public float Lives;
    public GameObject Ship;

    private GameObject SimpleShip;
    // Use this for initialization
    void Start()
    {
        SimpleShip = (GameObject)Instantiate(Ship);
        
    }

    // Update is called once per frame
    void Update()
    {
        if (SimpleShip == null)
        {
            Debug.Log("NULL");
            Lives--;
            SimpleShip = (GameObject)Instantiate(Ship);
        }
        if (Lives == 0)
        {
            SceneManager.LoadScene("GameOverScene");
        }
    }
}
