﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraPanning : MonoBehaviour
{
    public float ScrollSpeed;
    public int boundary;
    public float Speed;
    private float speed;
    public Vector3 pos;
    // Use this for initialization
    void Start()
    {

        pos = transform.position;
    }
    
    // Update is called once per frame
    void Update()
    {
        speed = Speed * Time.deltaTime;
        if (Input.mousePosition.x >= Screen.width - boundary)
        {
            pos.x += speed;
        }
        if (Input.mousePosition.x <= boundary)
        {
            pos.x -= speed;
        }
        if (Input.mousePosition.y >= Screen.height - boundary)
        {
            pos.z += speed;
        }
        if (Input.mousePosition.y <= boundary)
        {
            pos.z -= speed;
        }
        float scroll = Input.GetAxis("Mouse ScrollWheel");

        pos.y -= scroll * ScrollSpeed * Time.deltaTime;

        pos.y = Mathf.Clamp(pos.y, 5, 20);
        pos.x = Mathf.Clamp(pos.x, 10, 50);
        pos.z = Mathf.Clamp(pos.z, -5, 45);

        transform.position = pos;
    }
}
