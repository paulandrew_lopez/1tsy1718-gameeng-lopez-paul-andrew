﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    public Spawner spawn;
    public float Lives;
    public static int Gold = 500;

    private float MaxLives;
    [Header("Texts and Images")]
    public Text WaveNumber;
    public Text CurrentGold;
    public Image healthBar;

    public List<GameObject> HeroList;

    private int Fast = 3;
    private int Med = 2;
    private int Slow = 1;

  
    private float Range = 7;
  
    // Use this for initialization
    void Start()
    {
        SetHeroStats();
        MaxLives = Lives;
    }


    // Update is called once per frame
    void Update()
    {

        CurrentGold.text = Gold.ToString();
        WaveNumber.text = "Wave " + spawn.Wave.ToString();
        healthBar.fillAmount = Lives / MaxLives;
        if (Lives <= 0)
            Debug.Log("You died");
    }
    void SetHeroStats()
    {
        //Kila
        if (HeroList[0])
        {
            HeroList[0].AddComponent<HeroStats>();

            HeroList[0].GetComponent<HeroStats>().minDamage = 10;
            HeroList[0].GetComponent<HeroStats>().maxDamage = 12;    
            HeroList[0].GetComponent<HeroStats>().Range = Range;
            HeroList[0].GetComponent<HeroStats>().AttackSpeed = Fast;
        }
        //Koko
        if (HeroList[1])
        {
            HeroList[1].AddComponent<HeroStats>();

            HeroList[1].GetComponent<HeroStats>().minDamage = 45;
            HeroList[1].GetComponent<HeroStats>().maxDamage = 50;
            HeroList[1].GetComponent<HeroStats>().Range = Range;
            HeroList[1].GetComponent<HeroStats>().AttackSpeed = Slow;
        }
        //Kola
        if (HeroList[2])
        {
            HeroList[2].AddComponent<HeroStats>();
            
            HeroList[2].GetComponent<HeroStats>().minDamage = 25;
            HeroList[2].GetComponent<HeroStats>().maxDamage = 30;
            HeroList[2].GetComponent<HeroStats>().Range = Range;
            HeroList[2].GetComponent<HeroStats>().AttackSpeed = Med;
            
        }
    }
}
