﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MinimapController : MonoBehaviour {
    public CameraPanning CamPan;

    private Ray ray;
    private Vector3 newCamPos;
    private Vector3 viewPortPos;
    private float mainCamHeight;
    // Use this for initialization
    void Start () {
		
	}
   
    // Update is called once per frame
    void Update () {
        if (Input.GetMouseButtonDown(0))
        { // if left button pressed...
            viewPortPos = GetComponent<Camera>().ScreenToViewportPoint(Input.mousePosition);         
            ray = GetComponent<Camera>().ViewportPointToRay(viewPortPos);
            RaycastHit hit;
 
            if (Physics.Raycast(ray, out hit) /*&& hit.transform.name == "Minimap"*/)
            {

                // hit.point contains the point where the ray hits the
                // object named "MinimapBackground"
                
                //Debug.Log(hit.point);
                //Debug.Log(hit.transform.name);
                newCamPos = hit.point;
                mainCamHeight = Camera.main.transform.position.y;

                CamPan.pos = new Vector3(newCamPos.x, mainCamHeight, newCamPos.z);

                //Debug.Log(CamPan.pos);
            }
        }
        Debug.DrawRay(ray.origin, ray.direction * 100, Color.blue);
    }
}
