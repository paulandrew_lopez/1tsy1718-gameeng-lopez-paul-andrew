﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Burning : Debuff
{
    MonsterStats MonsStats;
    public float DamageAmount = 10f;
    float tickDamage = 1;
    private void Start()
    {
        MonsStats = GetComponent<MonsterStats>();
    }
    protected override void Update()
    {
        tickDamage -= Time.deltaTime;
        if (tickDamage <= 0)
        {
            Effect();
            tickDamage = 1;
        }
        if (MonsStats == null)
        {
            Destroy(this);
        }
        base.Update();

    }
    public override void Effect()
    {
        MonsStats.MonsterHP -= DamageAmount;
    }
}
