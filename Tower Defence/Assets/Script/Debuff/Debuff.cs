﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Debuff : MonoBehaviour {
    
    public float Duration = 10f;
    
    public float Timer;
    // Use this for initialization
    protected virtual void Update()
    {
        Timer += Time.deltaTime;
        
        if (Timer >= Duration)
        {
            Destroy(this);
        }
    }
    public virtual void Effect()
    {

    }
}
