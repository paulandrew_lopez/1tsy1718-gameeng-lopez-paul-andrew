﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Slow : Debuff {
    EnemyMovement enemMovement;
    private float SlowAmount = 2;
    // Use this for initialization
    private void Start()
    {
        enemMovement = GetComponent<EnemyMovement>();
        Effect();
    }
    protected override void Update ()
    {
        base.Update();
        if (enemMovement == null)
        {
            Destroy(this);          
        }
    }
	public override void Effect ()
	{
        if(enemMovement != null)
        enemMovement.agent.speed = enemMovement.agent.speed / SlowAmount;
	}
}
