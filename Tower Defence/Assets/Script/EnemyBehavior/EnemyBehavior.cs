﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyBehavior : MonoBehaviour
{

    public Image HPBar;

    private float Fill;
    
    // Use this for initialization
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //if (this.GetComponent<MonsterStats>().MonsterHP <= 0)
        //{
        //    MonsterDied();
        //}
   
        UpdateHealth();
    }
    void UpdateHealth()
    {
        Fill = this.GetComponent<MonsterStats>().MonsterHP / this.GetComponent<MonsterStats>().MonsterMaxHp;
        HPBar.fillAmount = Fill;
    }
   
    
    public void TakeDamage(float amount)
    {
        
        this.GetComponent<MonsterStats>().MonsterHP -= amount;


        if (this.GetComponent<MonsterStats>().MonsterHP <= 0)
        {
            MonsterDieded();
        }
    }
    void MonsterDieded()
    {
        this.GetComponent<MonsterStats>().isDead = true;

        GameController.Gold += this.GetComponent<MonsterStats>().Gold;

        Spawner.monsterCount--;

        Destroy(gameObject);
    }
}
