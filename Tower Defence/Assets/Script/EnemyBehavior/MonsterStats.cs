﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class MonsterStats : MonoBehaviour
{
    public enum MonsterType
    {
        Flying,
        Ground,
    }
    public MonsterType MonsType;
    public float MonsterMaxHp;
    public float MonsterHP;
    public int Gold;
    public bool isDead = false;

    // Use this for initialization
    void Start()
    {
        MonsterHP = MonsterMaxHp;
    }

    // Update is called once per frame
    void Update()
    {
        //if (Input.anyKey)
        //{
        //    MonsterHP--;
        //}
       
    }

}

