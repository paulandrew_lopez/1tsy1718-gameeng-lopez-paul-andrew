﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class BuildTower : MonoBehaviour
{
    Ray ray;
    RaycastHit hit;

    [SerializeField] GameObject prefabTower;
    [SerializeField] GameObject objectTower;
    public bool isBuilding;

    public List<GameObject> Turrets;
    public GameObject SelectedTower;

    public float BuildTime;
    public int Cost;

    GameObject twr;
    private void Start()
    {
      
        isBuilding = false;
    }
    // other ideas
    // spawn the object immediately then translate its Y coord upwards (so it looks like its rising from the ground)
    // on click of button sets TowerName to actual name of tower
    void SelectTower(string nem)
    {
        if (isBuilding == false)
        {
            if (nem == "Bow")
            {
                SelectedTower = Turrets[0];
            }
            if (nem == "Cannon")
            {             
                SelectedTower = Turrets[1];
            }
            if (nem == "Fire")
            {
                SelectedTower = Turrets[2];
            }
            if (nem == "Ice")
            {
                SelectedTower = Turrets[3];
            }
        }
    }
    void SetTowerStats(GameObject Tower, string Name)
    {
        Tower.AddComponent<TurretStats>();

        if (isBuilding == false)
        {
            if (Name == "Bow")
            {
                Tower.GetComponent<TurretStats>().BuildTime = 10f;
                Tower.GetComponent<TurretStats>().Cost = 100;
                Tower.GetComponent<TurretStats>().Range = 15f;
                Tower.GetComponent<TurretStats>().minDamage = 30f;
                Tower.GetComponent<TurretStats>().maxDamage = 50f;
                Tower.GetComponent<TurretStats>().FireRate = 2f;
            }
            if (Name == "Cannon")
            {
                Tower.GetComponent<TurretStats>().BuildTime = 15f;
                Tower.GetComponent<TurretStats>().Cost = 200;
                Tower.GetComponent<TurretStats>().Range = 10f;
                Tower.GetComponent<TurretStats>().minDamage = 20f;
                Tower.GetComponent<TurretStats>().maxDamage = 25f;
                Tower.GetComponent<TurretStats>().FireRate = 1f;
            }
            if (Name == "Fire")
            {
                Tower.GetComponent<TurretStats>().BuildTime = 30f;
                Tower.GetComponent<TurretStats>().Cost = 500;
                Tower.GetComponent<TurretStats>().Range = 10f;
                Tower.GetComponent<TurretStats>().minDamage = 10f;
                Tower.GetComponent<TurretStats>().maxDamage = 13f;
                Tower.GetComponent<TurretStats>().FireRate = 1.5f;
            }
            if (Name == "Ice")
            {
                Tower.GetComponent<TurretStats>().BuildTime = 30f;
                Tower.GetComponent<TurretStats>().Cost = 500;
                Tower.GetComponent<TurretStats>().Range = 10f;
                Tower.GetComponent<TurretStats>().minDamage = 10f;
                Tower.GetComponent<TurretStats>().maxDamage = 13f;
                Tower.GetComponent<TurretStats>().FireRate = 1.5f;
            }
            Cost = Tower.GetComponent<TurretStats>().Cost;
        }
    }

    public void CreateTower(string TowerName)
    {
        SelectTower(TowerName);
        
        if (SelectedTower != null && isBuilding == false)
        {
           
            if (Cost <= GameController.Gold)
            {
                twr = Instantiate(SelectedTower) as GameObject;
                //Debug.Log("i like my women the way i like my floods; hanggang tuhod lang");
                twr.name = TowerName;
                SetTowerStats(twr, TowerName);
                objectTower = twr;
            }
            else
                Debug.Log("Not enough dough");
            return;
        }
    }
    //void SetTowerStats(string TurretName)
    //{
    //    if (TurretName == "Bow" && )
    //    {
    //        Cost = 100;
    //        BuildTime = 10f;
    //        SelectedTower = Turrets[0];
    //    }
    //    if (TurretName == "Cannon" && isBuilding == false)
    //    {
    //        Cost = 300;
    //        BuildTime = 15f;
    //        SelectedTower = Turrets[1];
    //    }
    //    if (TurretName == "Fire" && isBuilding == false)
    //    {
    //        Cost = 500;
    //        BuildTime = 30f;
    //        SelectedTower = Turrets[2];
    //    }
    //    if (TurretName == "Ice" && isBuilding == false)
    //    {
    //        Cost = 500;
    //        BuildTime = 30f;
    //        SelectedTower = Turrets[3];
    //    }
    //}
    Vector3 SnapToGrid(Vector3 towerObject)
    {
        return new Vector3(Mathf.Round(towerObject.x),
                            towerObject.y,
                            Mathf.Round(towerObject.z));
    }
    void Update()
    {
        SelectArea();
        if (isBuilding)
        {
            //Debug.Log(twr.GetComponent<TurretStats>().BuildTime.ToString("N0"));
            twr.GetComponent<TurretStats>().BuildTime -= Time.deltaTime;

            if (twr.GetComponent<TurretStats>().BuildTime <= 0)
            {
                twr.GetComponent<Turret>().enabled = true;
                prefabTower.transform.position = new Vector3(prefabTower.transform.position.x, 2, prefabTower.transform.position.z);
                isBuilding = false;
            }
        }
    }
    void SelectArea()
    {
        if (objectTower != null)
        {
            twr.GetComponent<Turret>().enabled = false;
            ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit))
            {
                Vector3 towerPos = hit.point;
                objectTower.transform.position = SnapToGrid(towerPos);
                //Debug.Log(hit.point);
                if (hit.point.y > 2)
                {
                    objectTower.GetComponent<TowerScript>().Buildable();
                    if (Input.GetMouseButtonUp(0) && !EventSystem.current.IsPointerOverGameObject())
                    {
                       
                        isBuilding = true;

                        GameController.Gold -= twr.GetComponent<TurretStats>().Cost;
                        objectTower.GetComponent<TowerScript>().Built();

                        objectTower.transform.position = new Vector3(objectTower.transform.position.x, -2, objectTower.transform.position.z);

                        prefabTower = objectTower;
                        objectTower = null;
                    }
                }
                else
                    objectTower.GetComponent<TowerScript>().NonBuildable();
                Debug.DrawLine(ray.origin, hit.point, Color.red);
            }
        }
    }
}
