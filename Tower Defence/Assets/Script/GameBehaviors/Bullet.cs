﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    private Transform target;
    public GameObject twr;
    public Turret turret;
    public float ExplosionRadius = 0f;
    public float Speed = 70f;
    public void Seek(Transform _target)
    {
        //put effects here?
        target = _target;
    }
    // Update is called once per frame
    void Update()
    {
        if (target == null)
        {
            Destroy(gameObject);
            return;
        }
        if (twr.name.Contains("Bow"))
        {
            ExplosionRadius = 0;
        }
        Vector3 dir = target.position - transform.position;
        float distanceThisFrame = Speed * Time.deltaTime;

        if (dir.magnitude <= distanceThisFrame)
        {

            HitTarget();
            return;
        }
        transform.Translate(dir.normalized * distanceThisFrame, Space.World);

        transform.LookAt(target);
    }
    void HitTarget()
    {
        if (ExplosionRadius > 0f)
        {
            Explode();

        }
        else
        {
            //if (target.GetComponent<MonsterStats>().MonsType == MonsterStats.MonsterType.Ground || target.GetComponent<MonsterStats>().MonsType == MonsterStats.MonsterType.Flying)
            //{
                Damage(target);
            //}
        }
        Destroy(gameObject);
    }

    void Damage(Transform Enemy)
    {
        float randDamage = Random.Range(turret.GetComponent<TurretStats>().minDamage, turret.GetComponent<TurretStats>().maxDamage);

        EnemyBehavior e = Enemy.GetComponent<EnemyBehavior>();

        if (e != null)
        {
            e.TakeDamage(randDamage);
        }



    }
    void Explode()
    {
        Collider[] colliders = Physics.OverlapSphere(transform.position, ExplosionRadius);
        foreach (Collider col in colliders)
        {
            if (twr.name.Contains("Fire"))
            {
              
                    if (col.gameObject.GetComponent<Burning>() == null)
                    {
                        col.gameObject.AddComponent<Burning>();
                    }

                    else if (col.gameObject.GetComponent<Burning>() != null)
                    {
                        col.gameObject.GetComponent<Burning>().Timer = 0;
                    }
                Destroy(gameObject);
                Damage(col.transform);
            }
            if (twr.name.Contains("Ice"))
            {               
                    if (col.gameObject.GetComponent<Slow>() == null)
                    {
                        col.gameObject.AddComponent<Slow>();
                    }
                    else if (col.gameObject.GetComponent<Slow>() != null)
                    {
                        col.gameObject.GetComponent<Slow>().Timer = 0;
                    }
                Destroy(gameObject);
                Damage(col.transform);
            }
            if (twr.name.Contains("Cannon"))
            {
                if (target.GetComponent<MonsterStats>().MonsType == MonsterStats.MonsterType.Ground)
                {
                    Destroy(gameObject);
                    Damage(col.transform);
                }
                else if (target.GetComponent<MonsterStats>().MonsType == MonsterStats.MonsterType.Flying)
                {
                    Destroy(gameObject);
                }
            }          
        }
    }
    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, ExplosionRadius);
    }
}
