﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Colliders : MonoBehaviour
{
    public GameController controll;
    public Spawner spawn;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        //Test Code
        if (Input.GetKeyDown(KeyCode.A))
        {
            if (!this.gameObject.GetComponent<Burning>())
                this.gameObject.AddComponent<Burning>();
            else
            {
                DestroyImmediate(this.gameObject.GetComponent<Burning>());
                this.gameObject.GetComponent<Burning>();
            }
        }
    }
    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.name.Contains("Goblin") && this.gameObject.name.Contains("Core"))
        {
            Spawner.monsterCount--;
            controll.Lives--;
            Debug.Log("-1 Life");
            Destroy(col.gameObject);
        }
        if (col.gameObject.name.Contains("Raven") && this.gameObject.name.Contains("Core"))
        {
            Spawner.monsterCount--;
            controll.Lives--;
            Debug.Log("-1 Life");
            Destroy(col.gameObject);
        }
        if (col.gameObject.name.Contains("Dragon") && this.gameObject.name.Contains("Core"))
        {
            Spawner.monsterCount--;
            Debug.Log("Game Over");
            controll.Lives = 0;
            Destroy(col.gameObject);
        }

    }
}
