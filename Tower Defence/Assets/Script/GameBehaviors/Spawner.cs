﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class WaveData
{


}

public class Spawner : MonoBehaviour
{
    public enum MonsterType
    {
        Flying,
        Ground,
    }
    public enum State
    {
        Spawning,
        Waiting,
        Counting
    }
    [System.Serializable]
    public class MonsterData
    {
        public List<GameObject> monsterPrefab;
    }
    public int Wave;
    public int BossWave;

    public GameObject Boss;

    public float TimeTillNextWave;
    public float DefaultTimeForWave;

    public static int monsterCount;

    public MonsterData[] MonsterList;

    public List<GameObject> ListOfSpawnedMonsters;

    private int maxWave;
    private GameObject Monster;
    private State state = State.Counting;
    private bool Spawned = false;
    // Use this for initialization
    void Start()
    {
        TimeTillNextWave = DefaultTimeForWave;
    }
    void SetMonsterStats(int wave)
    {

        Monster.AddComponent<MonsterStats>();
        if (Monster.name.Contains("Goblin"))
            Monster.GetComponent<MonsterStats>().MonsType = MonsterStats.MonsterType.Ground;

        else if (Monster.name.Contains("Raven"))
            Monster.GetComponent<MonsterStats>().MonsType = MonsterStats.MonsterType.Flying;

        Monster.GetComponent<MonsterStats>().Gold = 30 * wave;
        Monster.GetComponent<MonsterStats>().MonsterMaxHp = 50 * wave;

    }
    void SetBossStats(int wave)
    {
        Boss.AddComponent<MonsterStats>();
        if (Boss.name.Contains("Dragon")) Boss.GetComponent<MonsterStats>().MonsType = MonsterStats.MonsterType.Flying;
        Boss.GetComponent<MonsterStats>().Gold += 100 * wave;
        Boss.GetComponent<MonsterStats>().MonsterMaxHp += 50 * wave;
    }
    IEnumerator SpawnMonster(MonsterData mons)
    {
        state = State.Spawning;
        if (Wave % BossWave == 0 && Spawned == false)
        {
            Spawned = true;
            Boss = Instantiate(Boss, this.transform.position, Quaternion.identity);

            SetBossStats(Wave);
            ListOfSpawnedMonsters.Add(Boss);
        }
        for (int i = 0; i < mons.monsterPrefab.Count; i++)
        {
            monsterCount++;
            Monster = Instantiate(mons.monsterPrefab[i], this.transform.position, Quaternion.identity);

            SetMonsterStats(Wave);
            ListOfSpawnedMonsters.Add(Monster);
            yield return new WaitForSeconds(0.0f);
        }
    }
    void Update()
    {
        if (monsterCount > 0) return;
        if (TimeTillNextWave <= 0)
        {
            state = State.Counting;
            if (state != State.Spawning)
            {
                if (Wave > MonsterList.Length) Wave = 0;
                Wave++;
                StartCoroutine(SpawnMonster(MonsterList[Wave]));
                TimeTillNextWave = DefaultTimeForWave;

            }
        }
        else
        {
            TimeTillNextWave -= Time.deltaTime;
        }
    }
}