﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Turret : MonoBehaviour
{
    private Transform target;

    private float fireCountdown = 0f;

    [Header("Unity Setup Fields")]
    public string EnemyTag = "Enemy";
    public float TurnSpeed = 10f;
    public Transform PartToRotate;

    public GameObject BulletPrefab;
    public Transform FirePoint;

    public GameObject TowerType;


    // Use this for initialization
    void Start()
    {
        InvokeRepeating("UpdateTarget", 0f, 0.5f);
    } 

    void UpdateTarget()
    {
        GameObject[] Enemies = GameObject.FindGameObjectsWithTag(EnemyTag);
        float shortestDistance = Mathf.Infinity;
        GameObject nearestEnemy = null;
        if (TowerType.name.Contains("Fire") || TowerType.name.Contains("Ice"))
        {
            foreach (GameObject enemy in Enemies)
            {
                float distanceToEnemy = Vector3.Distance(transform.position, enemy.transform.position);
                if (distanceToEnemy < shortestDistance)
                {
                    shortestDistance = distanceToEnemy;
                    nearestEnemy = enemy;
                }
            }
            if (nearestEnemy != null && shortestDistance <= this.GetComponent<TurretStats>().Range)
            {
                target = nearestEnemy.transform;
            }
            else
                target = null;
        }
        if (TowerType.name.Contains("Bow") || TowerType.name.Contains("Cannon"))
        {
            foreach (GameObject enemy in Enemies)
            {
                if (target != null)
                {
                    float distanceToTarget = Vector3.Distance(transform.position, enemy.transform.position);

                    if (distanceToTarget > this.GetComponent<TurretStats>().Range)
                    {
                        target = null;
                    }
                }
                if (target == null)
                {
                    float distanceToEnemy = Vector3.Distance(transform.position, enemy.transform.position);
                    if (distanceToEnemy <= this.GetComponent<TurretStats>().Range)
                    {
                        target = enemy.transform;
                    }
                }
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        
        if (target == null)
            return;
        Vector3 dir = target.position - transform.position;
        Quaternion lookRotation = Quaternion.LookRotation(dir);
        Vector3 Rotation = Quaternion.Lerp(PartToRotate.rotation, lookRotation, Time.deltaTime * TurnSpeed).eulerAngles;
        PartToRotate.rotation = Quaternion.Euler(0f, Rotation.y, 0f);

        if (fireCountdown <= 0f)
        {
            Shoot();
            fireCountdown = 1f / this.GetComponent<TurretStats>().FireRate;
        }
        fireCountdown -= Time.deltaTime;
    }
    void Shoot()
    {
        if (TowerType.name.Contains("Cannon") && target.GetComponent<MonsterStats>().MonsType == MonsterStats.MonsterType.Ground)
        {
            SeekTarget();
        }
        else if (!TowerType.name.Contains("Cannon"))
        {
            SeekTarget();
        }


    }
    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, this.GetComponent<TurretStats>().Range);
    }
    void SeekTarget()
    {
        GameObject BulletGameObj = (GameObject)Instantiate(BulletPrefab, FirePoint.position, FirePoint.rotation);
        Bullet bullet = BulletGameObj.GetComponent<Bullet>();
        bullet.turret = this;
        bullet.twr = TowerType;
        if (bullet != null)
        {
            bullet.Seek(target);
        }
    }
}
