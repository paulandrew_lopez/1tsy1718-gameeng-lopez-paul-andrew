﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum AnimationType
{
    Idle = 0,
    Run = 1,
    Attack = 2,
    Dead = 3
}

public class HeroAnim : MonoBehaviour
{
    public HeroMovement move;
    Animator anim;
    //[SerializeField] AnimationType animType = AnimationType.Idle;

    // Use this for initialization
    void Start()
    {
        anim = GetComponent<Animator>();
        move = GetComponent<HeroMovement>();
    }

    // Update is called once per frame
    void Update()
    {
        if (move.isMoving) anim.SetFloat("Movement", 1);
        else if (!move.isMoving) anim.SetFloat("Movement", 0);
    }
    //void OnChangeAnim(AnimationType animType)
    //{
    //    anim.SetInteger("AnimIndex", (int)animType);
    //}
}
