﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeroBullet : MonoBehaviour
{
    private Transform target;

    public HeroTargeting heroTarget;
    public float ExplosionRadius = 0f;
    public float Speed = 70f;
    public void Seek(Transform _target)
    {
        //put effects here?
        target = _target;
    }
    // Update is called once per frame
    void Update()
    {
        if (target == null)
        {
            Destroy(gameObject);
            return;
        }
        Vector3 dir = target.position - transform.position;
        float distanceThisFrame = Speed * Time.deltaTime;

        if (dir.magnitude <= distanceThisFrame)
        {
            HitTarget();
            return;
        }
        transform.Translate(dir.normalized * distanceThisFrame, Space.World);

        transform.LookAt(target);
    }
    void HitTarget()
    {

            Damage(target);

        
        Destroy(gameObject);
    }

    void Damage(Transform Enemy)
    {   
        float heroRandDamage = Random.Range(heroTarget.GetComponent<HeroStats>().minDamage, heroTarget.GetComponent<HeroStats>().maxDamage);
        EnemyBehavior e = Enemy.GetComponent<EnemyBehavior>();
    
     
            if (e != null)
            {
                e.TakeDamage(heroRandDamage);
            }
        

    }
    
}
