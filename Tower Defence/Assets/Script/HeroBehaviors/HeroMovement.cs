﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.AI;
using UnityEngine;

public class HeroMovement : MonoBehaviour
{
    public GameObject obj;
    public bool isMoving;


    private Vector3 TargetPos;
    private GameObject MoveMarking;
    private Vector3 dir;
    NavMeshAgent agent;
    // Use this for initialization
    void Start()
    {
        agent = this.GetComponent<NavMeshAgent>();
        TargetPos = transform.position;
        isMoving = false;  
    }

    // Update is called once per frame
    void Update()
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (Input.GetMouseButtonDown(1) && Physics.Raycast(ray, out hit))
        {
            Debug.DrawRay(ray.origin, ray.direction * 100, Color.blue);
            TargetPos = hit.point;

            Destroy(MoveMarking);
            MoveMarking = Instantiate(obj, hit.point, Quaternion.identity);
            isMoving = true;
        }

        agent.SetDestination(TargetPos);

        if (transform.position == agent.destination)
        {
            Destroy(MoveMarking);        
            isMoving = false;
        }
 
    }
}
