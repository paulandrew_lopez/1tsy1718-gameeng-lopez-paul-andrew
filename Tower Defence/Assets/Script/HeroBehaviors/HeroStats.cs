﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeroStats : MonoBehaviour {
    public int AttackSpeed;
    public float Range;
    public float minDamage;
    public float maxDamage;
    public List<SkillBase> SkillList;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
