﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeroTargeting : MonoBehaviour
{

    public GameObject HeroProjectile;
    public string EnemyTag = "Enemy";
    public Transform HeroFirePoint;
    public float randDamage;

    private float AttackCD = 0f;
    private Transform target;

    Animator anim;

    // Use this for initialization
    void Start()
    {
        anim = this.GetComponent<Animator>();
        InvokeRepeating("HeroUpdateTarget", 0f, 0.5f);
    }
   
    // Update is called once per frame
    void Update()
    {
        //Debug.Log(this.GetComponent<HeroStats>().maxDamage);
        //randDamage = Random.Range(GetComponent<HeroStats>().minDamage, GetComponent<HeroStats>().maxDamage);
        HeroMovement move = GetComponent<HeroMovement>();
        if (target == null || move.isMoving)
        {
            anim.SetFloat("Attack", 0);
            return;
        }

        if (AttackCD <= 0f)
        {
            Shoot();
            AttackCD = 1f / this.GetComponent<HeroStats>().AttackSpeed;
        }
        AttackCD -= Time.deltaTime;
    }
    void HeroUpdateTarget()
    {
        GameObject[] Enemies = GameObject.FindGameObjectsWithTag(EnemyTag);
        float shortestDistance = Mathf.Infinity;
        GameObject nearestEnemy = null;
        foreach (GameObject enemy in Enemies)
        {
            float distanceToEnemy = Vector3.Distance(transform.position, enemy.transform.position);
            if (distanceToEnemy < shortestDistance)
            {
                shortestDistance = distanceToEnemy;
                nearestEnemy = enemy;
            }
        }
        if (nearestEnemy != null && shortestDistance <= this.GetComponent<HeroStats>().Range)
        {
            
            target = nearestEnemy.transform;
        }
        else
            target = null;
    }
    void Shoot()
    {
        anim.SetFloat("Attack", 1);

        GameObject BulletGameObj = (GameObject)Instantiate(HeroProjectile, HeroFirePoint.position, transform.rotation);
        HeroBullet bullet = BulletGameObj.GetComponent<HeroBullet>();
        bullet.heroTarget = this;
        if (bullet != null)
        {

            bullet.Seek(target);
        }
    }
}
