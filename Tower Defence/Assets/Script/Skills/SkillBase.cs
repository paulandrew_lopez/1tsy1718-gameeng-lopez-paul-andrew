﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillBase : ScriptableObject {
    public float Cooldown;
    public float PercentDamage;
    public List<GameObject> Targets; 
    Ray ray;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	protected virtual void Update () {
		
	}
    public virtual void Use()
    {
        Debug.Log("Cast");
    }
}
