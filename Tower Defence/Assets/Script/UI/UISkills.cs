﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UISkills : MonoBehaviour {

    [SerializeField] GameObject heroObject;
    HeroStats hData;
    void Start()
    {
        hData = heroObject.GetComponent<HeroStats>();
        int i = 0;
        foreach (Transform child     in transform)
        {
            if (child.GetComponent<UISkillsButtons>())
            {
                child.GetComponent<UISkillsButtons>().skill = hData.SkillList[i];
            }
        }
    }
}
