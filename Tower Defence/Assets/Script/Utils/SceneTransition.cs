﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;


public class SceneTransition : MonoBehaviour
{

    public string HeroSelected;
    private int index = 1;
    private GameObject[] listOfChar;

    private void Start()
    {
        index = PlayerPrefs.GetInt("CharacterSelected");

        listOfChar = new GameObject[transform.childCount];

        for (int i = 0; i < transform.childCount; i++)
        {
            listOfChar[i] = transform.GetChild(i).gameObject;
        }
        foreach (GameObject obj in listOfChar)
        {
            obj.SetActive(false);
        }
        if (listOfChar[index])
        {
            listOfChar[index].SetActive(true);
        }

    }

    public void ToggleLeft()
    {
        listOfChar[index].SetActive(false);
        index--;
        if (index < 0) index = listOfChar.Length - 1;
        listOfChar[index].SetActive(true);
    }
    public void ToggleRight()
    {
        listOfChar[index].SetActive(false);
        index++;
        if (index == listOfChar.Length) index = 0;
        listOfChar[index].SetActive(true);
    }
    public void Confirm()
    {
        PlayerPrefs.SetInt("CharacterSelected", index);
        SceneManager.LoadScene("SampleRatio");
    }
}

